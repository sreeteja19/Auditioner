package com.game.core.Audition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DancersTest {

	Audition a;
	Participant p,p1;
	@Before
	public void setUp() {
		a=new Dancer();
		p=new Participant(11,"dancer" , "salsa",'\0', 0);
		
	}
	@Test
	public void testDancer() {
		String expected="salsa-11-dancer";
		String actual=a.play(p.getId(),p.getAuditioningFor(),p.getDanceStyle(),p.getKey(),p.getValue());
		assertEquals(expected, actual);
	}

}
