package com.game.core.Audition;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class ParticpantsListTest {
	ArrayList<Participant> list=new ArrayList<Participant>();
	ListOfParticipants lp;
	@Before
	public void setUp() {
		
		list.add(new Participant(11,"dancer" , "salsa",'\0', 0));
		list.add(new Participant(12,"performer" , null,'\0', 0));
		list.add(new Participant(156,"Vocalist" , null,'g', 3));
		list.add(new Participant(13,"performer" , null,'\0', 0));
		list.add(new Participant(121,"performer" ,null ,'\0', 0));
		list.add(new Participant(132,"dancer" , "classical",'\0', 0));
		list.add(new Participant(10,"performer" , null,'\0', 0));
		lp=new  ListOfParticipants();
		
	}
	@Test
	public void performerCountTest() {
		int expected=4;
		int actual=lp.totalPerformers(list);
		assertEquals(expected, actual);
		}
	@Test
	public void dancerCountTest() {
		int expected=2;
		int actual=lp.totalDancers(list);
		assertEquals(expected, actual);
			}
	@Test
	public void vocalistCountTest() {
		int expected=1;
		int actual=lp.totalVocalists(list);
		assertEquals(expected, actual);
	}
	@Test
	public void totalParticpantsTest() {
		int expected=7;
		int actual=lp.totalParticipants(list);
		assertEquals(expected, actual);
	}



	
}
