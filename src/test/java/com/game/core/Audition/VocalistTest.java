package com.game.core.Audition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VocalistTest {

	Audition a;
	Participant p,p1,p3;
	@Before
	public void setUp() {
		a=new Vocalists();
		p=new Participant(156,"Vocalist" ,null, 'g', 3);
		p1=new Participant(124,"Vocalist",null,'h',0);
		
	}
	@Test
	public void testVocalistVolume() {
		String expected="I sing in the key of -g- at the volume -3-156";
		String actual=a.play(p.getId(),p.getAuditioningFor(),p.getDanceStyle(),p.getKey(),p.getValue());
		assertEquals(expected, actual);
	}
	@Test
	public void testVocalist() {
		String expected="I sing in the key of -h-124";
		String actual=a.play(p1.getId(),p1.getAuditioningFor(),p1.getDanceStyle(),p1.getKey(),p1.getValue());
		assertEquals(expected, actual);
	}
	@Rule public ExpectedException exe=ExpectedException.none();
	@Test
	public void testException() {
		exe.expect(IllegalArgumentException.class);
		exe.expectMessage("volume should be between 1 and 10");
		new Participant(124,"Vocalist",null,'h',15);
		
	}
}
