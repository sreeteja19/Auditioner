package com.game.core.Audition;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PerformersTest {

	Audition a;
	Participant p;

	@Before
	public void setUp() {
		a = new Performers();
		p = new Participant(12, "performer", null, '\0', 0);
	}

	@Test
	public void testPerformer() {
		String expected = "12-performer";
		String actual = a.play(p.getId(), p.getAuditioningFor(), p.getDanceStyle(), p.getKey(), p.getValue());
		assertEquals(expected, actual);
	}

}
