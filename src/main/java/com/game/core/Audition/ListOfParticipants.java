package com.game.core.Audition;

import java.util.ArrayList;

public class ListOfParticipants {
	String participantId;

	public int totalParticipants(ArrayList<Participant> list) {
		return list.size();
	}

	public int totalPerformers(ArrayList<Participant> list) {
		int peformersCount = 0;
		for (Participant p : list) {
			if (p.getAuditioningFor().equalsIgnoreCase("performer")) {
				peformersCount++;
			}
		}
		return peformersCount;
	}

	public int totalDancers(ArrayList<Participant> list) {
		int dancersCount = 0;
		for (Participant p : list) {
			if (p.getAuditioningFor().equalsIgnoreCase("dancer")) {
				dancersCount++;
			}
		}
		return dancersCount;
	}

	public int totalVocalists(ArrayList<Participant> list) {
		int vocalistCount = 0;
		for (Participant p : list) {
			if (p.getAuditioningFor().equalsIgnoreCase("Vocalist")) {
				vocalistCount++;
			}
		}
		return vocalistCount;
	}

	public void performerList(ArrayList<Participant> list) {
		for (Participant p : list) {
			Audition audition = GetParticipantType.getType(p.getAuditioningFor());
			participantId = (audition.play(p.getId(), p.getAuditioningFor(), p.getDanceStyle(), p.getKey(),
					p.getValue()));
			System.out.println(participantId);
		}

	}
}
