package com.game.core.Audition;

public class Vocalists implements Audition {

	public String play(int id, String auditionType,String danceStyle,char key,int volume) {
		String vId=null;
		String text = "I sing in the key of -";
		if (volume>=1 && volume<=10) {
			vId = text + key + "- at the volume -" + volume + "-" + id;
		} else {
			vId = text + key + "-" + id;
		}
		
		return vId;
	}

}
