package com.game.core.Audition;

public interface Audition {
public String play(int id, String auditionType,String danceStyle,char key,int volume);
}
