package com.game.core.Audition;

public class Participant {
	private String auditioningFor;
	private char key;
	private int value;
	private String danceStyle;
	private int id;
	public Participant(int id,String auditioningFor,String danceStyle,char key,int value) {
		this.id=id;
		this.auditioningFor=auditioningFor;
		this.danceStyle=danceStyle;
		this.key=key;
		if(value<0||value>10) {
			throw new IllegalArgumentException("volume should be between 1 and 10");
		}
		this.value=value;
		}
	
	public char getKey() {
		return key;
	}
	public void setKey(char key) {
		this.key = key;
	}
		public String getAuditioningFor() {
		return auditioningFor;
	}
	public void setAuditioningFor(String auditioningFor) {
		this.auditioningFor = auditioningFor;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		
		this.value = value;
	
	}
	public String getDanceStyle() {
		return danceStyle;
	}
	public void setDanceStyle(String danceStyle) {
		this.danceStyle = danceStyle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
