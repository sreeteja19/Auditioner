package com.game.core.Audition;

public class GetParticipantType {
	public static Audition getType(String type) {
		if(type.equalsIgnoreCase("performer")){
			return new Performers();
		}
		else if(type.equalsIgnoreCase("dancer")) {
			return new Dancer();
		}
		else if(type.equalsIgnoreCase("Vocalist")) {
			return new Vocalists();
		}
		else {
			return null;
		}
		
	}

}
